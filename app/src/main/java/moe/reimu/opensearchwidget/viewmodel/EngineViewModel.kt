package moe.reimu.opensearchwidget.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.github.kittinunf.fuel.coroutines.awaitByteArray
import com.github.kittinunf.fuel.coroutines.awaitString
import com.github.kittinunf.fuel.httpGet
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import moe.reimu.opensearchwidget.App
import moe.reimu.opensearchwidget.OpenSearchEngine
import moe.reimu.opensearchwidget.db.Engine
import moe.reimu.opensearchwidget.parseDataUrl
import org.jsoup.Jsoup
import java.lang.Exception
import java.util.*
import java.util.regex.Pattern


class EngineViewModel(application: Application) : AndroidViewModel(application), CoroutineScope {
    private var parentJob = Job()
    override val coroutineContext
        get() = parentJob + Dispatchers.Main
    val engineDao = (application as App).db.engineDao()
    val engines: LiveData<List<OpenSearchEngine>>

    val urlPattern: Regex = Pattern.compile("""^https?://.*""").toRegex()

    init {
        engines = Transformations.map(engineDao.loadAll()) {
            it.map { engine ->
                OpenSearchEngine(engine.host, engine.spec, engine.icon)
            }
        }
    }

    suspend fun removeEngine(host: String) {
        engineDao.delete(engineDao.loadByHostSuspend(host)!!)
    }

    suspend fun addEngine(host: String) {
        val lowerHost = host.toLowerCase(Locale.getDefault())
        val baseUrl = if (urlPattern.matches(lowerHost)) {
            lowerHost
        } else {
            "https://$lowerHost/"
        }
        val isGoogle = host.contains("google")
        val specUrl = if (isGoogle) {
            "$baseUrl/searchdomaincheck?format=opensearch"
        } else {
            val htmlReq = baseUrl.httpGet()
            htmlReq.header("User-Agent", UA_CHROME)
            val htmlString = htmlReq.awaitString()
            val htmlDoc = Jsoup.parse(htmlString, baseUrl)
            val searchElem = htmlDoc.select("link[rel=search]").first()!!
            searchElem.attr("abs:href")
        }
        Log.d("OSE: Spec URL", specUrl)

        val spec = specUrl.httpGet().apply {
            header("User-Agent", UA_CHROME)
        }.awaitString()

        Log.d("OSE: Spec", spec)

        val engine = OpenSearchEngine(host, spec, null)
        val iconBytes = engine.iconUrl?.run {
            try {
                parseDataUrl(this) ?: this.httpGet().awaitByteArray()
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }
        val engineEntity = Engine()
        engineEntity.host = host
        engineEntity.spec = spec
        engineEntity.icon = iconBytes
        engineDao.insertEngine(engineEntity)
    }

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }

    companion object {
        const val UA_EDGE =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18362"
        const val UA_CHROME =
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36"
    }
}