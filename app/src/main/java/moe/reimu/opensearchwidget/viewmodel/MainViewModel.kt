package moe.reimu.opensearchwidget.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.shopify.livedataktx.combineWith
import com.shopify.livedataktx.debounce
import kotlinx.coroutines.*
import moe.reimu.opensearchwidget.*
import moe.reimu.opensearchwidget.providers.OpenSearch
import moe.reimu.opensearchwidget.providers.Provider
import java.lang.Exception


class MainViewModel(application: Application) : AndroidViewModel(application), CoroutineScope {
    private var parentJob = Job()
    override val coroutineContext
        get() = parentJob + Dispatchers.Main
    val engineDao = (application as App).db.engineDao()
    val defaultProvider: LiveData<Provider> by lazy {
        LiveSharedPreferences("default_host").combineWith(allProviders) { defaultHost, providers ->
            providers?.firstOrNull {
                it.host == defaultHost
            } ?: providers?.firstOrNull() as Provider?
        }
    }
    val allProviders by lazy {
        engineDao.loadAll().map { engines ->
            engines.map {
                OpenSearch(OpenSearchEngine(it.host, it.spec, it.icon))
            }
        }
    }
    var suggestionJob: Job? = null
    val suggestionsProgress = MutableLiveData<Boolean>().apply {
        value = false
    }
    val query = MutableLiveData<String>()

    val suggestions by lazy {
        defaultProvider.combineWith(query.debounce(500)) { p, q ->
            suggestionJob?.cancel()
            val ret = MutableLiveData<List<String>>()
            suggestionJob = launch(Dispatchers.IO) {
                suggestionsProgress.postValue(true)
                val _suggestions: List<String> = try {
                    if (q == null) {
                        emptyList()
                    } else {
                        p?.autocomplete(q) ?: emptyList()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    emptyList()
                }
                ret.postValue(_suggestions)
            }.apply {
                invokeOnCompletion {
                    suggestionsProgress.postValue(false)
                    suggestionJob = null
                }
            }
            ret
        }.switchMap { it }
    }
}