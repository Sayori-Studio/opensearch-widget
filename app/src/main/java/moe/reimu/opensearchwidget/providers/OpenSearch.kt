package moe.reimu.opensearchwidget.providers

import android.content.Context
import android.content.Intent
import android.util.Log
import com.github.kittinunf.fuel.coroutines.awaitObject
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.json.jsonDeserializer
import moe.reimu.opensearchwidget.OpenSearchEngine
import moe.reimu.opensearchwidget.Prefs
import moe.reimu.opensearchwidget.activity.SearchResultActivity
import splitties.intents.intent
import java.net.URL
import java.net.URLDecoder


fun URL.splitQuery(): Map<String, String> {
    val query_pairs = LinkedHashMap<String, String>()
    val query = this.query ?: return query_pairs
    val pairs = query.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    for (pair in pairs) {
        val idx = pair.indexOf("=")
        query_pairs[URLDecoder.decode(pair.substring(0, idx), "UTF-8")] =
                URLDecoder.decode(pair.substring(idx + 1), "UTF-8")
    }
    return query_pairs
}

class OpenSearch(private val engine: OpenSearchEngine): Provider {
    override var name = engine.name
    override var icon = engine.icon
    override var host = engine.host
    override fun searchIntent(context: Context, input: String): Intent {
        val url = engine.searchUrl!!
        val (replacedUrl, params) = url.replaceTerm(input)
        val mutParams = params.toMutableMap()
        mutParams.putAll(URL(replacedUrl).splitQuery())
        return SearchResultActivity.intent { _, extrasSpec ->
            extrasSpec.method = if (Prefs.forcePost) {
                "POST"
            } else {
                url.method
            }
            extrasSpec.url = replacedUrl
            extrasSpec.params = HashMap(mutParams)
        }
    }

    override suspend fun autocomplete(input: String): List<String> {
        val url = engine.autoCompleteUrl ?: return emptyList()
        val (replacedUrl, params) = url.replaceTerm(input)
        Log.d("OSE", replacedUrl)
        val result = when {
            url.method == "GET" -> replacedUrl.httpGet(params.toList())
            url.method == "POST" -> replacedUrl.httpPost(params.toList())
            else -> return emptyList()
        }.awaitObject(jsonDeserializer()).array()
        return parseXSuggestion(result)
    }

}