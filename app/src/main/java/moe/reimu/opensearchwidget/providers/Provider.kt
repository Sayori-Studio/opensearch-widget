package moe.reimu.opensearchwidget.providers

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import org.json.JSONArray


interface Provider {
    suspend fun autocomplete(input: String): List<String>
    fun searchIntent(context: Context, input: String) : Intent
    fun parseXSuggestion(result: JSONArray): List<String> {
        val arr = result.getJSONArray(1)
        val ret = mutableListOf<String>()
        for (i in 0 until arr.length()) {
            ret.add(arr[i].toString())
        }
        return ret
    }
    var name: String
    var host: String
    var icon: Bitmap?
}