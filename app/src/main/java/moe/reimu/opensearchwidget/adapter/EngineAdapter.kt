package moe.reimu.opensearchwidget.adapter

import android.content.Context
import android.view.View
import kotlinx.android.synthetic.main.item_engine.view.*
import moe.reimu.opensearchwidget.OpenSearchEngine
import moe.reimu.opensearchwidget.R

class EngineAdapter(context: Context) :
    ArrayAdapter<OpenSearchEngine, EngineAdapter.ViewHolder>(context, R.layout.item_engine) {
    override fun onCreateViewHolder(itemView: View) = ViewHolder(itemView)

    class ViewHolder(itemView: View): ArrayAdapter.ViewHolder<OpenSearchEngine>(itemView) {
        override fun bind(item: OpenSearchEngine) {
            itemView.text_name.text = item.name
            itemView.text_host.text = item.host
            itemView.image_autocomplete.visibility = if (item.autoCompleteUrl == null) View.GONE else View.VISIBLE
            if (item.icon != null) {
                itemView.image_icon.setImageBitmap(item.icon)
            } else {
                itemView.image_icon.visibility = View.GONE
            }
        }
    }
}
