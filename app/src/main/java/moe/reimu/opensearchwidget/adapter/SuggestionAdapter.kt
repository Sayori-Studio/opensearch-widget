package moe.reimu.opensearchwidget.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_suggestion.view.*
import moe.reimu.opensearchwidget.R



class SuggestionAdapter(context: Context) :
    ArrayAdapter<String, SuggestionAdapter.ViewHolder>(context, R.layout.item_suggestion) {
    override fun onCreateViewHolder(itemView: View) = ViewHolder(itemView)
    class ViewHolder(itemView: View): ArrayAdapter.ViewHolder<String>(itemView) {
        override fun bind(item: String) {
            itemView.suggestion_text.text = item
        }
    }
}
