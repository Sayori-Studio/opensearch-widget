package moe.reimu.opensearchwidget.adapter

import android.content.Context
import android.view.View
import kotlinx.android.synthetic.main.item_provider.view.*
import moe.reimu.opensearchwidget.R
import moe.reimu.opensearchwidget.providers.Provider


class ProviderAdapter(context: Context) :
    ArrayAdapter<Provider, ProviderAdapter.ViewHolder>(context, R.layout.item_provider) {
    override fun onCreateViewHolder(itemView: View) = ViewHolder(itemView)
    var defaultHost: String = ""
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.itemView.defaultIndicator.visibility = if (data[position].host == defaultHost) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
    class ViewHolder(itemView: View): ArrayAdapter.ViewHolder<Provider>(itemView) {
        override fun bind(item: Provider) {
            if (item.icon != null) {
                itemView.iconView.setImageBitmap(item.icon)
            } else {
                itemView.iconView.setImageResource(R.drawable.ic_puzzle_piece_silhouette)
            }

        }
    }
}
