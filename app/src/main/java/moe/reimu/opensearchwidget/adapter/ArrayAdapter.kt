package moe.reimu.opensearchwidget.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import moe.reimu.opensearchwidget.R


abstract class ArrayAdapter<T, VH: ArrayAdapter.ViewHolder<T>>(context: Context, private val layoutResId: Int):
    RecyclerView.Adapter<VH>() {
    var data = emptyList<T>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    var onClickListener: ((T) -> Unit)? = null
    var onLongClickListener: ((T) -> Unit)? = null

    abstract fun onCreateViewHolder(itemView: View): VH
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        onCreateViewHolder(
            layoutInflater.inflate(layoutResId, parent, false)
        )

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(data[position])
        holder.itemView.setOnClickListener {
            onClickListener?.invoke(data[position])
        }
        holder.itemView.setOnLongClickListener {
            onLongClickListener?.invoke(data[position])
            true
        }
    }

    abstract class ViewHolder<T>(itemView: View): RecyclerView.ViewHolder(itemView) {
        abstract fun bind(item: T)
    }
}