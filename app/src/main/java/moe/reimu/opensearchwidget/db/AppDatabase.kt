package moe.reimu.opensearchwidget.db

import androidx.room.Database
import androidx.room.RoomDatabase


@Database(entities = [Engine::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun engineDao(): EngineDao
}