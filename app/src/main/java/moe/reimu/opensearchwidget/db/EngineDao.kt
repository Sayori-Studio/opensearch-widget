package moe.reimu.opensearchwidget.db

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface EngineDao {
    @Query("SELECT * FROM engine")
    fun loadAll(): LiveData<List<Engine>>

    @Query("SELECT * FROM engine WHERE host=:host")
    fun loadByHost(host: String): LiveData<Engine?>

    @Query("SELECT * FROM engine WHERE host=:host")
    suspend fun loadByHostSuspend(host: String): Engine?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEngine(engine: Engine)

    @Delete
    suspend fun delete(engine: Engine)
}