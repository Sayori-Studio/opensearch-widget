package moe.reimu.opensearchwidget.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Engine {
    @PrimaryKey
    var host: String = ""
    var spec: String = ""
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    var icon: ByteArray? = null
}