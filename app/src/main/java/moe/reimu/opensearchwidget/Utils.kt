package moe.reimu.opensearchwidget

import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.Base64
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import splitties.init.appCtx


fun parseDataUrl(url: String): ByteArray? {
    val reg = """^data:(.+);base64,([a-zA-Z0-9+/]+={0,2})$""".toRegex(RegexOption.IGNORE_CASE)
    val matchResult = reg.matchEntire(url) ?: return null
    val b64 = matchResult.destructured.component2()
    return Base64.decode(b64, Base64.DEFAULT)
}


class LiveSharedPreferences(private val prefKey: String): LiveData<String>() {
    private val prefs = PreferenceManager.getDefaultSharedPreferences(appCtx)
    private val mListener = SharedPreferences.OnSharedPreferenceChangeListener {
            _, key ->
        if (key == prefKey) {
            value = prefs.getString(prefKey, "")
        }
    }

    override fun onActive() {
        super.onActive()
        value = prefs.getString(prefKey, "")
        prefs.registerOnSharedPreferenceChangeListener(mListener)
    }

    override fun onInactive() {
        super.onInactive()
        prefs.unregisterOnSharedPreferenceChangeListener(mListener)
    }
}

fun<X, Y> LiveData<X>.switchMap(func: (X) -> LiveData<Y>) = Transformations.switchMap(this, func)!!
fun<X, Y> LiveData<X>.map(func: (X) -> Y) = Transformations.map(this, func)!!