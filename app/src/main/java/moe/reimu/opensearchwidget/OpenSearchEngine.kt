package moe.reimu.opensearchwidget

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import divstar.ico4a.codec.ico.ICODecoder
import org.w3c.dom.Document
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import org.xml.sax.InputSource
import java.io.ByteArrayInputStream
import java.io.StringReader
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory


class OpenSearchEngine(val host: String, spec: String, iconBytes: ByteArray?) {
    var searchUrl: OpenSearchUrl? = null
    var autoCompleteUrl: OpenSearchUrl? = null
    val name: String
    var icon: Bitmap? = null
    var iconUrl: String? = null
    init {
        val doc = readXml(spec)
        val xpFactory = XPathFactory.newInstance()
        val xPath = xpFactory.newXPath()

        val nameNode = xPath.evaluate("/OpenSearchDescription/ShortName", doc, XPathConstants.NODE) as Node
        name = nameNode.textContent
        val urlNodes = xPath.evaluate("/OpenSearchDescription/Url", doc, XPathConstants.NODESET) as NodeList
        for (i in 0 until urlNodes.length) {
            val node = urlNodes.item(i)
            val attrs = node.attributes
            val mime = attrs.getNamedItem("type") ?: continue
            when (mime.textContent) {
                "application/x-suggestions+json" -> {
                    autoCompleteUrl = OpenSearchUrl(node)
                }
                "text/html" -> {
                    searchUrl = OpenSearchUrl(node)
                }
            }
        }
        val imageNode = xPath.evaluate("/OpenSearchDescription/Image", doc, XPathConstants.NODE)
        if (imageNode != null) {
            iconUrl = (imageNode as Node).textContent
        }
        if (iconBytes != null) {
            if (iconBytes.sliceArray(0..4).contentEquals(byteArrayOf(0, 0, 1, 0))) {
                val images = ICODecoder.read(ByteArrayInputStream(iconBytes))
                if (images != null && images.size > 0) icon = images[0]
            } else {
                icon = BitmapFactory.decodeByteArray(iconBytes, 0, iconBytes.size)
            }
        }
    }

    private fun readXml(input: String): Document {
        val dbFactory = DocumentBuilderFactory.newInstance()
        val dBuilder = dbFactory.newDocumentBuilder()
        val xmlInput = InputSource(StringReader(input))
        return dBuilder.parse(xmlInput)
    }
}