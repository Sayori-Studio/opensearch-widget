package moe.reimu.opensearchwidget

import org.w3c.dom.Node
import org.w3c.dom.NodeList
import java.net.URLEncoder
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory


class OpenSearchUrl(urlNode: Node) {
    val template: String
    val params: Map<String, String>
    val method: String
    init {
        val xPath = XPathFactory.newInstance().newXPath()
        val attrs = urlNode.attributes
        template = attrs.getNamedItem("template").textContent
        method = attrs.getNamedItem("method")?.textContent?.toUpperCase() ?: "GET"
        val _params = mutableMapOf<String, String>()
        val paramNodes = xPath.evaluate("//Url/Param", urlNode, XPathConstants.NODESET) as NodeList
        for (i in 0 until paramNodes.length) {
            val paramAttrs = paramNodes.item(i).attributes
            _params[paramAttrs.getNamedItem("name").textContent] = paramAttrs.getNamedItem("value").textContent
        }
        params = _params.toMap()
    }

    fun replaceTerm(searchTerm: String): Pair<String, Map<String, String>> {
        val replacedUrl = template.replace(SEARCH_TERMS, URLEncoder.encode(searchTerm, "UTF-8"))
            .replace(INPUT_ENCODING, "UTF-8")
            .replace(INPUT_ENCODING_OPT, "UTF-8")
            .replace(OUTPUT_ENCODING, "UTF-8")
            .replace(OUTPUT_ENCODING_OPT, "UTF-8")
        val params = params.mapValues {
            it.value.replace(SEARCH_TERMS, searchTerm)
            it.value.replace(INPUT_ENCODING, "UTF-8")
            it.value.replace(OUTPUT_ENCODING, "UTF-8")
        }
        return Pair(replacedUrl, params)
    }

    companion object {
        const val SEARCH_TERMS = "{searchTerms}"
        const val INPUT_ENCODING = "{inputEncoding}"
        const val OUTPUT_ENCODING = "{outputEncoding}"
        const val INPUT_ENCODING_OPT = "{inputEncoding?}"
        const val OUTPUT_ENCODING_OPT = "{outputEncoding?}"
    }
}