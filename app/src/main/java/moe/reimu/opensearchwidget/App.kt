package moe.reimu.opensearchwidget

import android.app.Application
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import moe.reimu.opensearchwidget.db.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import moe.reimu.opensearchwidget.db.Engine
import java.nio.charset.Charset

class App : Application() {
    fun readAsset(path: String): ByteArray {
        val stream = assets.open(path)
        val buffer = ByteArray(stream.available())
        stream.read(buffer)
        return buffer
    }

    val dbCallback = object : RoomDatabase.Callback() {
        override fun onCreate(_db: SupportSQLiteDatabase) {
            super.onCreate(_db)
            GlobalScope.launch(Dispatchers.IO) {
                val engineDao = db.engineDao()
                val ddg = Engine()
                ddg.host = "duckduckgo.com"
                ddg.spec = readAsset("duckduckgo.xml").toString(Charset.forName("UTF-8"))
                ddg.icon = readAsset("duckduckgo.ico")
                engineDao.insertEngine(ddg)
                Prefs.defaultHost = "duckduckgo.com"
            }
        }
    }
    val db: AppDatabase by lazy {
        Room.databaseBuilder(
            this,
            AppDatabase::class.java, "db"
        )
            .addCallback(dbCallback)
            .build()
    }
}