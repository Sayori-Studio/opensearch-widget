package moe.reimu.opensearchwidget.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_search_result.*
import moe.reimu.opensearchwidget.Prefs
import moe.reimu.opensearchwidget.R
import splitties.bundle.BundleSpec
import splitties.bundle.bundle
import splitties.bundle.withExtras
import splitties.intents.ActivityIntentSpec
import splitties.intents.activitySpec
import java.net.URI
import java.net.URLEncoder


class SearchResultActivity : AppCompatActivity() {
    companion object : ActivityIntentSpec<SearchResultActivity, ExtrasSpec> by activitySpec(ExtrasSpec)
    object ExtrasSpec : BundleSpec() {
        var url: String by bundle()
        var method: String by bundle()
        var params: HashMap<String, String> by bundle()
    }

    lateinit var host: String
    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_result)
        withExtras(ExtrasSpec) {
            host = URI(url).host
            if (method == "POST") {
                web.postUrl(getUrlWithoutParameters(url), buildQuery(params).toByteArray())
            }
            if (method == "GET") {
                web.loadUrl(getUrlWithoutParameters(url) + "?" + buildQuery(params))
            }
        }
        web.webChromeClient = webChromeClient
        web.webViewClient = webViewClient
        web.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        setSupportActionBar(toolbar)
    }

    val webChromeClient = object : WebChromeClient() {
        override fun onReceivedTitle(view: WebView, title: String) {
            super.onReceivedTitle(view, title)
            if (title.isNotEmpty()) {
                this@SearchResultActivity.title = title
            }
            supportActionBar?.subtitle = view.url
        }

        override fun onProgressChanged(view: WebView, newProgress: Int) {
            super.onProgressChanged(view, newProgress)
            if (newProgress < 100 && progress_bar.visibility == View.GONE) {
                progress_bar.visibility = View.VISIBLE
            }
            progress_bar.progress = newProgress
            if (newProgress == 100) {
                progress_bar.visibility = View.GONE
            }
        }

    }

    val webViewClient = object : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            return if (request.url.host == host) {
                false
            } else {
                startActivity(Intent(Intent.ACTION_VIEW, request.url))
                true
            }
        }
    }

    override fun onBackPressed() {
        if (web.copyBackForwardList().currentIndex > 0) {
            web.goBack()
        } else {
            super.onBackPressed()
        }
    }

    private fun buildQuery(map: Map<String, String>): String {
        val rawBody = StringBuilder()
        for (param in map) {
            rawBody.append(URLEncoder.encode(param.key, "UTF-8"))
            rawBody.append("=")
            rawBody.append(URLEncoder.encode(param.value, "UTF-8"))
            rawBody.append("&")
        }
        return rawBody.toString()
    }

    private fun getUrlWithoutParameters(url: String): String {
        val uri = URI(url)
        return URI(
            uri.scheme,
            uri.authority,
            uri.path,
            null, // Ignore the query part of the input url
            uri.fragment
        ).toString()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (Prefs.clearCache) {
            clearCookies()
            web.clearCache(true)
            web.clearHistory()
        }
    }

    @Suppress("DEPRECATION")
    fun clearCookies() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null)
            CookieManager.getInstance().flush()
        } else {
            val cookieSyncMgr = CookieSyncManager.createInstance(this)
            cookieSyncMgr.startSync()
            val cookieManager = CookieManager.getInstance()
            cookieManager.removeAllCookie()
            cookieManager.removeSessionCookie()
            cookieSyncMgr.stopSync()
            cookieSyncMgr.sync()
        }
    }
}
