package moe.reimu.opensearchwidget.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import moe.reimu.opensearchwidget.*
import moe.reimu.opensearchwidget.adapter.ProviderAdapter
import moe.reimu.opensearchwidget.adapter.SuggestionAdapter
import moe.reimu.opensearchwidget.viewmodel.MainViewModel
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView

class MainActivity : AppCompatActivity() {
    private val suggestionAdapter by lazy {
        SuggestionAdapter(this).apply {
            onClickListener = {
                searchTextView.text = SpannableStringBuilder(it)
                searchTextView.setSelection(it.length)
            }
        }
    }
    private val providerAdapter by lazy {
        ProviderAdapter(this).apply {
            onClickListener = {
                val query = searchTextView.text.toString()
                if (query.isNotBlank()) {
                    val intent = it.searchIntent(this@MainActivity, query)
                    startActivity(intent)
                }
            }
            onLongClickListener = {
                Prefs.defaultHost = it.host
                Toast
                    .makeText(this@MainActivity, getString(R.string.default_set_to, it.name), Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    val mainViewModel by lazy {
        ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    private val watcher = object: TextWatcher {
        override fun afterTextChanged(s: Editable?) = Unit
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            updateButtonIcon()
            val searchText = s.toString().trim()
            if (searchText.isBlank()) {
                suggestionAdapter.data = emptyList()
                mainViewModel.suggestionJob?.cancel()
                return
            }
            mainViewModel.query.value = searchText
        }

    }

    fun updateButtonIcon() {
        val resId = if (searchTextView.text.isEmpty()) {
            R.drawable.ic_arrow_back_black_24dp
        } else {
            R.drawable.ic_clear_black_24dp
        }
        if (backButton.tag != resId) {
            backButton.setImageResource(resId)
            backButton.tag = resId
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        searchTextView.addTextChangedListener(watcher)

        searchTextView.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val query = searchTextView.text.toString()
                if (query.isBlank()) return@setOnEditorActionListener false
                val provider = mainViewModel.defaultProvider.value
                provider?.apply {
                    startActivity(searchIntent(this@MainActivity, query))
                }
                true
            } else {
                false
            }
        }

        searchSuggestions.apply {
            setHasFixedSize(true)
            adapter = suggestionAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

        providers.apply {
            setHasFixedSize(true)
            adapter = providerAdapter
            layoutManager = LinearLayoutManager(this@MainActivity, RecyclerView.HORIZONTAL, false)
            addItemDecoration(DividerItemDecoration(
                context,
                RecyclerView.HORIZONTAL
            ))
        }

        settingsButton.setOnClickListener {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }

        backButton.setOnClickListener {
            if (searchTextView.text.isEmpty()) {
                finish()
            } else {
                searchTextView.text.clear()
            }
        }

        mainViewModel.suggestions.observe(this, Observer {
            if (searchTextView.text.isBlank()) return@Observer
            suggestionAdapter.data = it
        })
        mainViewModel.allProviders.observe(this, Observer {
            providerAdapter.data = it
        })
        mainViewModel.defaultProvider.observe(this, Observer {
            it?.apply {
                providerAdapter.defaultHost = host
            }
        })
        mainViewModel.suggestionsProgress.observe(this, Observer {
            progressBar.visibility = if (it) {
                View.VISIBLE
            } else {
                View.GONE
            }
        })

        MaterialShowcaseView.Builder(this)
            .setTarget(providers)
            .setDismissText("GOT IT")
            .setContentText("Long press any search engine to set it as default")
            .setDelay(1000)
            .singleUse("DEFAULT_ENGINE")
            .show()
    }

    override fun onResume() {
        super.onResume()
        if (searchTextView.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }
}
