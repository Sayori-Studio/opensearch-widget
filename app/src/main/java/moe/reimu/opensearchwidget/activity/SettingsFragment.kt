package moe.reimu.opensearchwidget.activity


import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.Intent
import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import moe.reimu.opensearchwidget.R


class SettingsFragment : PreferenceFragmentCompat() {
    var appWidgetId: Int = AppWidgetManager.INVALID_APPWIDGET_ID

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
        appWidgetId = activity?.intent?.extras?.getInt(
            AppWidgetManager.EXTRA_APPWIDGET_ID,
            AppWidgetManager.INVALID_APPWIDGET_ID
        ) ?: AppWidgetManager.INVALID_APPWIDGET_ID

        val resultValue = Intent().apply {
            putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
        }
        activity?.setResult(Activity.RESULT_OK, resultValue)
    }
}
