package moe.reimu.opensearchwidget.activity

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import kotlinx.android.synthetic.main.activity_engine.*
import kotlinx.android.synthetic.main.item_engine.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moe.reimu.opensearchwidget.*
import moe.reimu.opensearchwidget.adapter.EngineAdapter
import moe.reimu.opensearchwidget.viewmodel.EngineViewModel
import java.lang.Exception

class EngineActivity : AppCompatActivity() {
    val engineViewModel by lazy {
        ViewModelProvider(this).get(EngineViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_engine)
        val engineAdapter = EngineAdapter(this)
        recycler_view.apply {
            adapter = engineAdapter
            layoutManager = LinearLayoutManager(this@EngineActivity)
            val swipeHandler = object : SwipeToDeleteCallback(this@EngineActivity) {
                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    val vh = viewHolder as EngineAdapter.ViewHolder
                    val host = vh.itemView.text_host.text.toString()
                    engineViewModel.launch(Dispatchers.IO) {
                        engineViewModel.removeEngine(host)
                    }
                }
            }
            ItemTouchHelper(swipeHandler).attachToRecyclerView(this)
        }
        engineViewModel.engines.observe(this, Observer {
            engineAdapter.data = it
        })

        fab.setOnClickListener { showNewDialog() }
    }

    fun showNewDialog() {
        MaterialDialog(this).show {
            input(hintRes = R.string.new_engine_dialog_msg) { _, host ->
                Snackbar.make(
                    this@EngineActivity.recycler_view,
                    R.string.fetching_info, Snackbar.LENGTH_SHORT
                ).show()

                engineViewModel.launch {
                    var message = getString(R.string.add_success)
                    try {
                        withContext(Dispatchers.IO) {
                            engineViewModel.addEngine(host.toString())
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        message = getString(R.string.add_failed, e.message ?: "")
                    }
                    Snackbar.make(this@EngineActivity.recycler_view, message, Snackbar.LENGTH_SHORT)
                        .show()
                }
            }
            positiveButton(android.R.string.ok)
        }
    }
}
