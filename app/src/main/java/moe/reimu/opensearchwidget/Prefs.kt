package moe.reimu.opensearchwidget

import splitties.experimental.ExperimentalSplittiesApi
import splitties.preferences.DefaultPreferences


@UseExperimental(ExperimentalSplittiesApi::class)
object Prefs: DefaultPreferences() {
    var forcePost by BoolPref("force_post", false)
    var clearCache by BoolPref("clear_cache", false)
    var defaultHost by StringPref("default_host", "")
}