package moe.reimu.opensearchwidget

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import moe.reimu.opensearchwidget.activity.MainActivity
import splitties.intents.toPendingActivity

class SearchWidget : AppWidgetProvider() {

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    companion object {
        internal fun updateAppWidget(
            context: Context, appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ) {
            val views = RemoteViews(context.packageName, R.layout.widget)
            val intent = Intent(context, MainActivity::class.java).toPendingActivity()
            views.setOnClickPendingIntent(R.id.search_widget, intent)
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }
}

